import setuptools


setuptools.setup(
    name='social-video',
    version='0.1.0',
    description='A webapp to watch video and share screenshots and clips with friends.',
    author='Kyle Wilcox',
    author_email='kyle@kwil.co',
    packages=setuptools.find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
        ],
    },
    install_requires=(
        'Django',
        'sentry-sdk',
        'djangorestframework',
    ),
)
