FROM python:3.8


# ffmpeg for moviepy
RUN apt-get update && apt-get install --yes ffmpeg


# web server
RUN pip install uwsgi


# application code
WORKDIR /app

# install requirements first for nice caching
COPY requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

COPY . /app
RUN pip install .


EXPOSE 5000
CMD ["uwsgi", "--http", ":5000", "--wsgi-file", "/app/socialvideo/wsgi.py", "--master", "--processes", "4" ]
# CMD ./manage.py runserver 0.0.0.0:5000