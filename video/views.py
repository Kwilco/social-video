from rest_framework import viewsets
from . import models, serializers

import django.http


def index(request):
    return django.http.HttpResponse("Video page index")


class ScreenshotViewSet(viewsets.ModelViewSet):
    queryset = models.Screenshot.objects.all().order_by('-created')
    serializer_class = serializers.ScreenshotSerializer


class ClipViewSet(viewsets.ModelViewSet):
    queryset = models.Clip.objects.all()
    serializer_class = serializers.ClipSerializer
