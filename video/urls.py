import django.urls

from rest_framework import routers
from . import views

router = routers.DefaultRouter()
router.register(r'screenshots', views.ScreenshotViewSet)
router.register(r'clips', views.ClipViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    # django.urls.path("", views.index, name="index"),
    django.urls.path('', django.urls.include(router.urls)),
    django.urls.path('api-auth/', django.urls.include('rest_framework.urls', namespace='rest_framework'))
]
