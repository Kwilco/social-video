import django.contrib.admin

from . import models


django.contrib.admin.site.register(models.Clip)
django.contrib.admin.site.register(models.ClipComment)
django.contrib.admin.site.register(models.Screenshot)
django.contrib.admin.site.register(models.ScreenshotComment)
