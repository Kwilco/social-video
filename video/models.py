from django.db import models
import django.contrib.auth.models


class LifecycleMetadata(models.Model):
    class Meta:
        abstract = True

    created = models.DateTimeField("creation timestamp", auto_now_add=True)
    modified = models.DateTimeField("last modified timestamp", auto_now=True)
    deleted = models.BooleanField("is deleted?", default=False)

    author = models.ForeignKey(
        to=django.contrib.auth.models.User, on_delete=models.PROTECT
    )

    @property
    def edited(self) -> bool:
        return self.created != self.modified


class Screenshot(LifecycleMetadata):
    """ A saved screenshot from an episode. """

    episode = models.TextField("episode code")
    timestamp = models.DecimalField("episode timecode", max_digits=10, decimal_places=4)

    subtitles = models.BooleanField("show subtitles", default=False)


class Clip(LifecycleMetadata):
    """ A saved clip from an episode. """

    episode = models.TextField("episode code")
    timestamp = models.DecimalField("episode timecode", max_digits=10, decimal_places=4)
    length = models.DecimalField("clip length", max_digits=10, decimal_places=4)

    sound = models.BooleanField("sound on", default=False)
    subtitles = models.BooleanField("show subtitles", default=False)


class ClipComment(LifecycleMetadata):
    text = models.TextField()
    parent = models.ForeignKey(to=Clip, on_delete=models.PROTECT)


class ScreenshotComment(LifecycleMetadata):
    text = models.TextField()
    parent = models.ForeignKey(to=Screenshot, on_delete=models.PROTECT)
